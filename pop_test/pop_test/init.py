def __init__(hub):
    hub.pop.sub.add(dyne_name="runner")
    hub.pop.sub.add(dyne_name="tests")
    hub.pop.sub.load_subdirs(hub.tests, recurse=True)


def cli(hub):
    hub.pop.config.load(["pop_test", "tests"], cli="pop_test")

    # Your app's options can now be found under hub.OPT.pop_test
    opts = dict(hub.OPT.pop_test)

    # Initialize the asyncio event loop
    hub.pop.loop.create()

    # Start the async code
    coroutine = hub.pop_test.init.run(**opts)
    hub.pop.Loop.run_until_complete(coroutine)


async def run(hub, **opts):
    """
    This is the entrypoint for the async code in your project
    """
    await hub.pop_test.session.apply(**opts)
