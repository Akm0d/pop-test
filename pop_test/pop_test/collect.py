from typing import List
import re

DEFAULT_FUNC_PATTERN = r"^test_.*"
DEFAULT_MOD_PATTERN = r"^test_.*"


def recurse(hub, sub: "pop.hub.Sub") -> List[str]:
    """
    Recursively collect all the test refs in the sub
    """
    refs = []

    # Recursively check for more subs in the directory
    for mod in sub._subs.values():
        new_refs = hub.pop_test.collect.recurse(mod)
        refs.extend(new_refs)

    # We have reached a python file
    for mod_name, mod in sub._loaded.items():
        # Only load modules as tests if they match the pattern
        if not re.match(DEFAULT_MOD_PATTERN, mod_name):
            continue

        # Check every function in the python file
        for func_name, func in mod._funcs.items():
            # TODO don't collect modules alphabetically, determine which ones are declared first in the file
            # Only load functions as tests if they match the pattern
            if not re.match(DEFAULT_FUNC_PATTERN, func_name):
                continue
            refs.append(f"{func.ref}.{func_name}")

    return refs
