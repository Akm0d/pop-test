
async def single(hub, run_name: str, ref: str):
    # Run a single test on the hub
    try:
        await hub.pop.loop.wrap(hub[ref])
        print(f"{ref}: PASSED")
    except Exception as e:
        msg = f"{e.__class__.__name__}: {e}"
        print(f"{ref}: FAILED: {msg}")
        hub.pop_test.RUNS[run_name]["errors"].append(msg)
