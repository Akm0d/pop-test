def __init__(hub):
    hub.pop_test.RUNS = {}


def create(hub, run_name: str, **opts):
    """
    Initialize the parameters for test run on the RUNS structure
    """
    hub.pop_test.RUNS[run_name] = {"opts": opts}
    hub.pop_test.RUNS[run_name]["refs"] = hub.pop_test.collect.recurse(sub=hub.tests)
    hub.pop_test.RUNS[run_name]["errors"] = []


async def apply(hub, run_name: str, runner_plugin: str = "serial", **opts):
    hub.pop_test.session.create(run_name=run_name, **opts)
    await hub.runner[runner_plugin].apply(run_name)

