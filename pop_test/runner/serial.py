"""
Run tests in the order they are defined
"""


async def apply(hub, run_name: str):
    refs = hub.pop_test.RUNS[run_name]["refs"]
    for ref in refs:
        await hub.pop_test.run.single(run_name, ref)

