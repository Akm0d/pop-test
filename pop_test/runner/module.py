"""
Run modules in parallel but the individual functions in the order they are defined
"""


async def apply(hub, run_name: str):
    ...
