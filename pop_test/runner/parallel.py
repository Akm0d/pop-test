"""
Run tests all at once
"""
import asyncio


async def apply(hub, run_name: str):
    refs = hub.pop_test.RUNS[run_name]["refs"]
    coroutines = []
    for ref in refs:
        coroutines.append(hub.pop_test.run.single(run_name, ref))

    for coro in asyncio.as_completed(*coroutines):
        await coro

